import * as userServices from './users';
import * as postServices from './posts';

export default {
  ...userServices,
  ...postServices,
};
