import axios from 'config/api';

/**
 * Get list of users
 * @param {object} params
 * @returns {Promise}
 */
export function fetchUsers(params) {
  return axios.get('users', { params }).then((response) => response.data);
}

/**
 * Get single user
 * @param {string} userId
 * @returns {Promise}
 */
export function fetchUserById(userId) {
  return axios.get(`users/${userId}`).then((response) => response.data);
}

/**
 * Store user in db
 * @param {object} body
 * @returns {Promise}
 */
export function storeUser(body) {
  return axios.post('users', body).then((response) => response.data);
}

/**
 * Update user in db
 * @param {object} body
 * @returns {Promise}
 */
export function updateUser(body) {
  return axios
    .patch(`users/${body.id}`, body)
    .then((response) => response.data);
}

/**
 * Delete single user
 * @param {string} userId
 * @returns {Promise}
 */
export function deleteUser(userId) {
  return axios.delete(`users/${userId}`).then((response) => response.data);
}
