import axios from 'config/api';

/**
 * Get list of posts
 * @param {object} params
 * @returns {Promise}
 */
export function fetchPosts(params) {
  return axios.get('posts', { params }).then((response) => response.data);
}

/**
 * Get single post
 * @param {string} postId
 * @returns {Promise}
 */
export function fetcPostById(postId) {
  return axios.get(`posts/${postId}`).then((response) => response.data);
}

/**
 * Store post in db
 * @param {object} body
 * @returns {Promise}
 */
export function storePost(body) {
  return axios.post('posts', body).then((response) => response.data);
}

/**
 * Update post in db
 * @param {object} body
 * @returns {Promise}
 */
export function updatePost(body) {
  return axios
    .patch(`posts/${body.id}`, body)
    .then((response) => response.data);
}

/**
 * Delete post
 * @param {string} postId
 * @returns {Promise}
 */
export function deletePost(postId) {
  return axios.delete(`posts/${postId}`).then((response) => response.data);
}
