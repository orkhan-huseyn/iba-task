import React from 'react';
import { NavLink } from 'react-router-dom';

import logo from 'assets/logo.svg';
import './styles.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function Layout({ children }) {
  return (
    <React.Fragment>
      <header className="header">
        <h1 className="sr-only">Task for International Bank Of Azerbaijan</h1>
        <NavLink title="Go to home page" className="header__logo" to="/">
          <img src={logo} alt="ibar logo" />
        </NavLink>
        <FontAwesomeIcon icon={['fas', 'user']} />
      </header>
      <main className="main">{children}</main>
      <footer className="footer">
        <p className="footer_copyright">
          © 2020 <a href="httsp://orkhan-huseyn.github.io">Orkhan Huseynli</a>.
          All Rights Reserved.
        </p>
      </footer>
    </React.Fragment>
  );
}

export default Layout;
