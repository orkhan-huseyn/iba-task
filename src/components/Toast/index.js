import React from 'react';
import classNames from 'classnames';

import './styles.scss';

export default function Toast({ show, message }) {
  return <div className={classNames('toast', { show: show })}>{message}</div>;
}
