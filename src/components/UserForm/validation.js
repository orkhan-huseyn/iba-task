/** formik validation function  */
export function validateForm(values) {
  const errors = {};

  if (!values.first_name) {
    errors.first_name = 'First name is required';
  }

  if (!values.last_name) {
    errors.last_name = 'Last name is required';
  }

  if (!values.gender) {
    errors.gender = 'Gender is required';
  }

  if (!values.dob) {
    errors.dob = 'Date of birth is required';
  }

  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }

  if (!values.phone) {
    errors.phone = 'Phone is required';
  }

  if (!values.website) {
    errors.website = 'Website is required';
  } else if (
    !/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/i.test(
      values.website
    )
  ) {
    errors.website = 'Website URL is not valid';
  }

  if (!values.address) {
    errors.address = 'Address is required';
  }

  if (!values.status) {
    errors.status = 'Status is required';
  }

  return errors;
}
