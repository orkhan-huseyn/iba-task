import React from 'react';
import { useHistory } from 'react-router-dom';
import { Formik } from 'formik';
import classNames from 'classnames';
import { validateForm } from './validation';

import './styles.scss';

export default function UserForm({ initialValues, requestFn }) {
  const history = useHistory();

  return (
    <div className="form-container">
      <Formik
        initialValues={initialValues}
        validate={validateForm}
        onSubmit={(values, { setSubmitting }) => {
          requestFn(values, () => {
            setSubmitting(false);
          });
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form className="form" onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="first_name">First name</label>
              <input
                id="first_name"
                type="text"
                name="first_name"
                placeholder="Enter first name"
                className={classNames({
                  'has-error': errors.first_name && touched.first_name,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.first_name}
              />
              <small>
                {errors.first_name && touched.first_name && errors.first_name}
              </small>
            </div>

            <div className="form-group">
              <label htmlFor="last_name">Last name</label>
              <input
                id="last_name"
                type="text"
                name="last_name"
                placeholder="Enter last name"
                className={classNames({
                  'has-error': errors.last_name && touched.last_name,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.last_name}
              />
              <small>
                {errors.last_name && touched.last_name && errors.last_name}
              </small>
            </div>

            <div className="form-group">
              <label htmlFor="gender">Gender</label>
              <select
                name="gender"
                id="gender"
                className={classNames({
                  'has-error': errors.gender && touched.gender,
                })}
                value={values.gender}
                onChange={handleChange}
                onBlur={handleBlur}
              >
                <option value="" disabled>
                  Select gender
                </option>
                <option value="male">Male</option>
                <option value="female">Female</option>
              </select>
              <small>{errors.gender && touched.gender && errors.gender}</small>
            </div>

            <div className="form-group">
              <label htmlFor="dob">Date of birth</label>
              <input
                id="dob"
                type="date"
                name="dob"
                placeholder="Select date of birth"
                className={classNames({
                  'has-error': errors.dob && touched.dob,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.dob}
              />
              <small>{errors.dob && touched.dob && errors.dob}</small>
            </div>

            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                id="email"
                type="email"
                name="email"
                placeholder="Enter email address"
                className={classNames({
                  'has-error': errors.email && touched.email,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <small>{errors.email && touched.email && errors.email}</small>
            </div>

            <div className="form-group">
              <label htmlFor="phone">Phone</label>
              <input
                id="phone"
                type="text"
                name="phone"
                placeholder="Enter phone number"
                className={classNames({
                  'has-error': errors.phone && touched.phone,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone}
              />
              <small>{errors.phone && touched.phone && errors.phone}</small>
            </div>

            <div className="form-group">
              <label htmlFor="website">Website</label>
              <input
                id="website"
                type="text"
                name="website"
                placeholder="Enter website"
                className={classNames({
                  'has-error': errors.website && touched.website,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.website}
              />
              <small>
                {errors.website && touched.website && errors.website}
              </small>
            </div>

            <div className="form-group">
              <label htmlFor="address">Address</label>
              <input
                id="address"
                type="text"
                name="address"
                placeholder="Enter address"
                className={classNames({
                  'has-error': errors.address && touched.address,
                })}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.address}
              />
              <small>
                {errors.address && touched.address && errors.address}
              </small>
            </div>

            <div className="form-group">
              <label htmlFor="status">Status</label>
              <select
                name="status"
                id="status"
                placeholder="Select status"
                className={classNames({
                  'has-error': errors.status && touched.status,
                })}
                value={values.status}
                onChange={handleChange}
                onBlur={handleBlur}
              >
                <option value="" disabled>
                  Select status
                </option>
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
              </select>
              <small>{errors.status && touched.status && errors.status}</small>
            </div>

            <div className="form-footer">
              <button
                className="btn btn--primary"
                style={{ marginRight: '1rem', marginTop: '1rem' }}
                type="submit"
                disabled={isSubmitting}
              >
                {isSubmitting ? 'Submitting...' : 'Save'}
              </button>
              <button
                type="button"
                style={{ marginTop: '1rem' }}
                onClick={() => history.push('/users')}
                className="btn"
              >
                Go back
              </button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
}
