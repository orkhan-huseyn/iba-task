import React, { useEffect, useRef } from 'react';

import './styles.scss';

export default function Modal({ isOpen, title, children, onClose }) {
  let closeBtnRef = useRef(null);

  useEffect(() => {
    if (isOpen && closeBtnRef.current) closeBtnRef.current.focus();
  }, [isOpen]);

  return (
    <div className="modal" style={{ display: isOpen ? 'block' : 'none' }}>
      <div className="modal-content">
        <div className="modal-header">
          <button ref={closeBtnRef} onClick={onClose} className="close">
            &times;
          </button>
          <h2>{title}</h2>
        </div>
        <div className="modal-body">{children}</div>
      </div>
    </div>
  );
}
