import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory } from 'react-router-dom';

export default function BackButton() {
  const history = useHistory();

  function onBackClick() {
    history.push('/users');
  }

  return (
    <button onClick={onBackClick} title="Back to users page" className="btn">
      <FontAwesomeIcon
        style={{ marginRight: '5px' }}
        icon={['fas', 'arrow-left']}
      />
      <span style={{ display: 'inline-block' }}>Back to users</span>
    </button>
  );
}
