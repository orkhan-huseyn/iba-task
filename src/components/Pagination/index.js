import React from 'react';
import ReactPaginate from 'react-paginate';

import './styles.scss';

export default function Pagination({ pageCount, onPageChange, currentPage }) {
  return (
    pageCount > 1 && (
      <ReactPaginate
        breakLabel="..."
        breakClassName="break"
        pageCount={pageCount}
        forcePage={currentPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={onPageChange}
        containerClassName="pagination"
        subContainerClassName="pages pagination"
        activeClassName="active"
      />
    )
  );
}
