import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEye,
  faTrash,
  faHome,
  faPlus,
  faUser,
  faEdit,
  faArrowLeft,
} from '@fortawesome/free-solid-svg-icons';

library.add(faEye, faTrash, faHome, faPlus, faUser, faEdit, faArrowLeft);
