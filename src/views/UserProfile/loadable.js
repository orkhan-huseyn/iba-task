import React from 'react';

const UserProfile = React.lazy(() => import('./index'));

export default function LoadableUserProfile() {
  return (
    <React.Suspense fallback={<h2>Loading component...</h2>}>
      <UserProfile />
    </React.Suspense>
  );
}
