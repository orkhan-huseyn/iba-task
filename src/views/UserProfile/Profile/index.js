import React, { useState, useEffect, useCallback } from 'react';
import { useParams } from 'react-router-dom';

import API from 'api';
import Toast from 'components/Toast';
import UserInfo from './UserInfo';

export default function Profile() {
  const params = useParams();
  const [user, setUser] = useState(null);
  const [message, setMessage] = useState('');

  const getUser = useCallback(() => {
    API.fetchUserById(params.userId)
      .then((response) => {
        const { success, message } = response._meta;
        if (success) {
          setUser(response.result);
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not fetch user info');
      });
  }, [params.userId]);

  useEffect(() => {
    getUser();
  }, [getUser]);

  useEffect(() => {
    if (!!message) {
      setTimeout(() => {
        setMessage('');
      }, 3000);
    }
  }, [message]);

  return (
    <React.Fragment>
      <Toast show={!!message} message={message} />
      <UserInfo user={user} />
    </React.Fragment>
  );
}
