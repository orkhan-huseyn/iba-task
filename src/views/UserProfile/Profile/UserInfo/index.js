import React from 'react';

import './styles.scss';

const placeholderUrl =
  'https://seedlingsgardening.com/wp-content/uploads/sites/9/2016/11/avatar.jpg';

export default function UserInfo({ user }) {
  return user ? (
    <div className="user-info">
      <div className="user-info__avatar">
        <img
          src={
            user._links.avatar.href ? user._links.avatar.href : placeholderUrl
          }
          alt={`${user.first_name}'s avatar`}
        />
        <h3>
          {user.first_name} {user.last_name}
        </h3>
      </div>

      <p className="user-info__item">
        Gender: <span className="value">{user.gender}</span>
      </p>
      <p className="user-info__item">
        Date of birth: <span className="value">{user.dob}</span>
      </p>
      <p className="user-info__item">
        Email: <span className="value">{user.email}</span>
      </p>
      <p className="user-info__item">
        Phone: <span className="value">{user.phone}</span>
      </p>
      <p className="user-info__item">
        Address: <span className="value">{user.address}</span>
      </p>
      <p className="user-info__item">
        Website:{' '}
        <span className="value">
          <a target="_blank" rel="noopener noreferrer" href={user.website}>
            {user.website}
          </a>
        </span>
      </p>
      <p className="user-info__item">
        Status:{' '}
        <span
          className={`badge ${
            user.status === 'active' ? 'badge--success' : 'badge--danger'
          }`}
        >
          {user.status}
        </span>
      </p>
    </div>
  ) : (
    <div style={{ textAlign: 'center' }}>
      <h4>Loading user info...</h4>
    </div>
  );
}
