import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './styles.scss';

export default function PostList({
  posts,
  totalCount,
  isLoading,
  onShowMore,
  onEditClick,
  onDeleteClick,
}) {
  return (
    <ul className="post-list">
      {!isLoading && posts.length === 0 ? (
        <li className="no-content">No posts found</li>
      ) : null}
      {posts.map((post) => (
        <li className="post-list__item" key={post.id}>
          <span className="post-list__item__header">
            <h4>{post.title}</h4>
            <button
              onClick={() => onEditClick(post)}
              type="button"
              style={{ marginRight: '5px', marginLeft: 'auto' }}
              title="Edit post"
              className="btn"
            >
              <FontAwesomeIcon
                style={{ fontSize: '1rem' }}
                icon={['fa', 'edit']}
              />
            </button>
            <button
              onClick={() => onDeleteClick(post.id)}
              type="button"
              title="Delete post"
              className="btn btn--danger"
            >
              <FontAwesomeIcon
                style={{ color: '#ffffff', fontSize: '1rem' }}
                icon={['fa', 'trash']}
              />
            </button>
          </span>

          <p>{post.body}</p>
        </li>
      ))}
      {!isLoading && posts.length < totalCount ? (
        <li className="load-more">
          <button
            onClick={onShowMore}
            type="button"
            className="btn"
            title="Load more posts"
          >
            Load more
          </button>
        </li>
      ) : null}
      {isLoading ? <li className="loading">Loading posts...</li> : null}
    </ul>
  );
}
