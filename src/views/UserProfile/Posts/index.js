import React, { useState, useEffect, useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useParams } from 'react-router-dom';

import Toast from 'components/Toast';
import Modal from 'components/Modal';
import API from 'api';
import List from './List';
import Form from './Form';

import './styles.scss';

export default function Posts() {
  const params = useParams();

  const [posts, setPosts] = useState([]);
  const [message, setMessage] = useState('');
  const [selectedPost, setSelectedPost] = useState({
    id: '',
    user_id: params.userId,
    title: '',
    body: '',
  });
  const [filters, setFilters] = useState({ title: '', body: '' });
  const [totalCount, setTotalCount] = useState(0);
  const [currenPage, setCurrentPage] = useState(1);
  const [isLoading, setLoading] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);

  const getUserPosts = useCallback(() => {
    setLoading(true);
    API.fetchPosts({
      user_id: params.userId,
      page: currenPage,
      ...filters,
    })
      .then((response) => {
        const { success, totalCount, message } = response._meta;
        if (success) {
          setPosts((posts) => [...posts, ...response.result]);
          setTotalCount(totalCount);
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not fetch posts');
      })
      .finally(() => {
        setLoading(false);
      });
  }, [currenPage, params.userId, filters]);

  useEffect(() => {
    getUserPosts();
  }, [getUserPosts]);

  useEffect(() => {
    if (!!message) {
      setTimeout(() => {
        setMessage('');
      }, 3000);
    }
  }, [message]);

  function saveOrUpdatePost(values, callback) {
    const request = values.id ? API.updatePost : API.storePost;

    const { id, ...rest } = values;
    let requestBody = rest;
    if (id) requestBody['id'] = id;

    request(requestBody)
      .then((response) => {
        const { success } = response._meta;
        if (success) {
          setModalOpen(false);
          setSelectedPost({
            id: '',
            user_id: params.userId,
            title: '',
            body: '',
          });
          setPosts([]);
          getUserPosts();
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage(`Could not ${id ? 'update' : 'insert'} post`);
      })
      .finally(() => {
        callback();
      });
  }

  function onShowMore() {
    setCurrentPage((currenPage) => currenPage + 1);
  }

  function onModalOpen() {
    setModalOpen(true);
    setSelectedPost({ id: '', user_id: params.userId, title: '', body: '' });
  }

  function onModalClose() {
    setModalOpen(false);
  }

  function onEditClick(post) {
    const { _links, ...theRest } = post;
    setSelectedPost(theRest);
    setModalOpen(true);
  }

  function onDeleteClick(postId) {
    API.deletePost(postId)
      .then((response) => {
        const { success, message } = response._meta;
        if (success) {
          setPosts([]);
          getUserPosts();
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not delete post');
      });
  }

  function onFilterChange(e) {
    if (e.key === 'Enter') {
      setFilters({
        ...filters,
        [e.target.name]: e.target.value,
      });
      setPosts([]);
    }
  }

  return (
    <div className="posts">
      <Toast show={!!message} message={message} />

      <Modal title="Add new post" isOpen={isModalOpen} onClose={onModalClose}>
        <Form
          initialValues={selectedPost}
          onModalClose={onModalClose}
          requestFn={saveOrUpdatePost}
        />
      </Modal>

      <div className="posts__header">
        <h3>User's posts</h3>
        <button
          onClick={onModalOpen}
          title="Add new post"
          className="btn btn--primary"
        >
          <FontAwesomeIcon
            style={{ marginRight: '5px' }}
            icon={['fas', 'plus']}
          />
          <span style={{ display: 'inline-block' }}>Add post</span>
        </button>
      </div>

      <div className="posts__filters">
        <input
          onKeyDown={onFilterChange}
          style={{ marginRight: '1rem' }}
          type="text"
          name="title"
          placeholder="Search by title"
          aria-label="Search posts by title"
        />
        <input
          onKeyDown={onFilterChange}
          type="text"
          name="body"
          placeholder="Search by content"
          aria-label="Search posts by content"
        />
      </div>

      <div className="posts__body">
        <List
          posts={posts}
          totalCount={totalCount}
          isLoading={isLoading}
          onShowMore={onShowMore}
          onEditClick={onEditClick}
          onDeleteClick={onDeleteClick}
        />
      </div>
    </div>
  );
}
