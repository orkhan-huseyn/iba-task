import React from 'react';
import { Formik } from 'formik';
import classNames from 'classnames';

function validateForm(values) {
  let errors = {};

  if (!values.title) {
    errors.title = 'Title is required';
  }

  if (!values.body) {
    errors.body = 'Content is required';
  }

  return errors;
}

export default function Form({ initialValues, requestFn, onModalClose }) {
  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validate={validateForm}
      onSubmit={(values, { setSubmitting }) => {
        requestFn(values, () => {
          setSubmitting(false);
        });
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form className="form" onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              id="title"
              type="text"
              name="title"
              placeholder="Enter title"
              className={classNames({
                'has-error': errors.title && touched.title,
              })}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.title}
            />
            <small>{errors.title && touched.title && errors.title}</small>
          </div>

          <div className="form-group">
            <label htmlFor="body">Content</label>
            <textarea
              id="body"
              type="text"
              name="body"
              rows={10}
              placeholder="Enter content"
              className={classNames({
                'has-error': errors.body && touched.body,
              })}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.body}
            ></textarea>
            <small>{errors.body && touched.body && errors.body}</small>
          </div>

          <div className="form-footer">
            <button
              className="btn btn--primary"
              style={{ marginRight: '1rem', marginTop: '1rem' }}
              type="submit"
              disabled={isSubmitting}
            >
              {isSubmitting ? 'Submitting...' : 'Save'}
            </button>
            <button
              type="button"
              style={{ marginTop: '1rem' }}
              onClick={onModalClose}
              className="btn"
            >
              Close
            </button>
          </div>
        </form>
      )}
    </Formik>
  );
}
