import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink } from 'react-router-dom';

import BackButton from 'components/BackButton';
import Profile from './Profile';
import Posts from './Posts';

import './styles.scss';

function UserProfile() {
  return (
    <React.Fragment>
      <h3 className="page-title">User profile and posts</h3>

      <div className="page-header d-flex-between">
        <ol className="breadcrump">
          <li className="breadcrump__item">
            <FontAwesomeIcon icon={['fas', 'home']} />
          </li>
          <li className="breadcrump__item">
            <NavLink to="/users">Users</NavLink>
          </li>
          <li className="breadcrump__item">User Profile</li>
        </ol>

        <BackButton />
      </div>

      <div className="page-body">
        <div className="profile">
          <div className="box">
            <Profile />
          </div>
        </div>
        <div className="posts">
          <div className="box">
            <Posts />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default UserProfile;
