import React from 'react';

const UserCreate = React.lazy(() => import('./index'));

export default function LoadableUserCreate() {
  return (
    <React.Suspense fallback={<h2>Loading component...</h2>}>
      <UserCreate />
    </React.Suspense>
  );
}
