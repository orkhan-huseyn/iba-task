import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink, useHistory } from 'react-router-dom';

import Toast from 'components/Toast';
import BackButton from 'components/BackButton';
import UserForm from 'components/UserForm';
import API from 'api';

function UserCreate() {
  const history = useHistory();
  const [message, setMessage] = useState('');

  useEffect(() => {
    if (!!message) {
      setTimeout(() => {
        setMessage('');
      }, 3000);
    }
  }, [message]);

  function insertUser(values, callback) {
    API.storeUser(values)
      .then((response) => {
        const { success, message } = response._meta;
        if (success) {
          history.push('/users');
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not insert user');
      })
      .finally(() => {
        callback();
      });
  }

  return (
    <React.Fragment>
      <Toast show={!!message} message={message} />

      <h3 className="page-title">Create new user</h3>

      <div className="page-header d-flex-between">
        <ol className="breadcrump">
          <li className="breadcrump__item">
            <FontAwesomeIcon icon={['fas', 'home']} />
          </li>
          <li className="breadcrump__item">
            <NavLink to="/users">Users</NavLink>
          </li>
          <li className="breadcrump__item">New User</li>
        </ol>

        <BackButton />
      </div>

      <div className="page-body">
        <UserForm
          requestFn={insertUser}
          initialValues={{
            first_name: '',
            last_name: '',
            gender: '',
            dob: '',
            email: '',
            phone: '',
            website: '',
            address: '',
            status: '',
          }}
        />
      </div>
    </React.Fragment>
  );
}

export default UserCreate;
