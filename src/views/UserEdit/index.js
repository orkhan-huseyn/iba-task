import React, { useCallback, useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NavLink, useHistory, useParams } from 'react-router-dom';

import Toast from 'components/Toast';
import BackButton from 'components/BackButton';
import UserForm from 'components/UserForm';
import API from 'api';

function UserEdit() {
  const history = useHistory();
  const params = useParams();

  const [message, setMessage] = useState('');
  const [user, setUser] = useState(null);

  const getUser = useCallback(() => {
    API.fetchUserById(params.userId)
      .then((response) => {
        const { success, message } = response._meta;
        if (success) {
          const { _links, ...theRest } = response.result;
          setUser(theRest);
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not fetch user info');
      });
  }, [params.userId]);

  useEffect(() => {
    getUser();
  }, [getUser]);

  useEffect(() => {
    if (!!message) {
      setTimeout(() => {
        setMessage('');
      }, 3000);
    }
  }, [message]);

  function updateUser(values, callback) {
    API.updateUser(values)
      .then((response) => {
        const { success, message } = response._meta;
        if (success) {
          history.push('/users');
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not update user');
      })
      .finally(() => {
        callback();
      });
  }

  return (
    <React.Fragment>
      <Toast show={!!message} message={message} />

      <h3 className="page-title">Update user info</h3>

      <div className="page-header d-flex-between">
        <ol className="breadcrump">
          <li className="breadcrump__item">
            <FontAwesomeIcon icon={['fas', 'home']} />
          </li>
          <li className="breadcrump__item">
            <NavLink to="/users">Users</NavLink>
          </li>
          <li className="breadcrump__item">Edit User</li>
        </ol>

        <BackButton />
      </div>

      <div className="page-body">
        {user ? (
          <UserForm requestFn={updateUser} initialValues={user} />
        ) : (
          <div style={{ textAlign: 'center', marginTop: '10rem' }}>
            <h4>Loading user info...</h4>
          </div>
        )}
      </div>
    </React.Fragment>
  );
}

export default UserEdit;
