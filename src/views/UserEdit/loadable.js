import React from 'react';

const UserEdit = React.lazy(() => import('./index'));

export default function LoadableUserEdit() {
  return (
    <React.Suspense fallback={<h2>Loading component...</h2>}>
      <UserEdit />
    </React.Suspense>
  );
}
