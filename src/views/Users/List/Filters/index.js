import React from 'react';

export default function Filters({ onFilterChange }) {
  return (
    <tr>
      <td></td>
      <td>
        <input
          type="text"
          name="first_name"
          aria-label="Search by first name"
          placeholder="First name"
          onKeyDown={onFilterChange}
        />
      </td>
      <td>
        <input
          type="text"
          name="last_name"
          aria-label="Search by last name"
          placeholder="Last name"
          onKeyDown={onFilterChange}
        />
      </td>
      <td>
        <select
          name="gender"
          aria-label="Search by gender"
          onChange={onFilterChange}
        >
          <option value="">All</option>
          <option value="male">Male</option>
          <option value="female">Female</option>
        </select>
      </td>
      <td>
        <input
          type="date"
          name="dob"
          placeholder="Birth date"
          aria-label="Search by birthdate"
          onChange={onFilterChange}
        />
      </td>
      <td>
        <input
          type="email"
          name="email"
          placeholder="Email"
          aria-label="Search by email"
          onKeyDown={onFilterChange}
        />
      </td>
      <td>
        <input
          type="text"
          name="phone"
          placeholder="Phone"
          aria-label="Search by phone"
          onKeyDown={onFilterChange}
        />
      </td>
      <td>
        <select
          name="status"
          aria-label="Search by status"
          onChange={onFilterChange}
        >
          <option value="">All</option>
          <option value="active">Active</option>
          <option value="inactive">Inactive</option>
        </select>
      </td>
      <td></td>
    </tr>
  );
}
