import React from 'react';
import Spinner from 'components/Spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Filters from './Filters';

export default function List({
  users,
  isLoading,
  offset,
  onViewClick,
  onEditClick,
  onDeleteClick,
  onFilterChange,
}) {
  return (
    <table>
      <thead>
        <Filters onFilterChange={onFilterChange} />
        <tr>
          <th>#</th>
          <th>First name</th>
          <th>Last name</th>
          <th>Gender</th>
          <th>Birth date</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {users.length === 0 && !isLoading ? (
          <tr>
            <td style={{ textAlign: 'center' }} colSpan={11}>
              No users found
            </td>
          </tr>
        ) : null}
        {isLoading ? (
          <tr>
            <td style={{ textAlign: 'center' }} colSpan={11}>
              <Spinner />
            </td>
          </tr>
        ) : null}
        {!isLoading
          ? users.map((user, i) => (
              <tr key={user.id}>
                <td>{offset + i + 1}</td>
                <td>{user.first_name}</td>
                <td>{user.last_name}</td>
                <td>{user.gender}</td>
                <td className="nowrap">{user.dob}</td>
                <td>{user.email}</td>
                <td>{user.phone}</td>
                <td>
                  <span
                    className={`badge ${
                      user.status === 'active'
                        ? 'badge--success'
                        : 'badge--danger'
                    }`}
                  >
                    {user.status}
                  </span>
                </td>
                <td className="nowrap">
                  <button
                    onClick={() => onViewClick(user.id)}
                    type="button"
                    style={{ marginRight: '5px' }}
                    title="View user's profile"
                    className="btn"
                  >
                    <FontAwesomeIcon icon={['fa', 'eye']} />
                  </button>
                  <button
                    onClick={() => onEditClick(user.id)}
                    type="button"
                    style={{ marginRight: '5px' }}
                    title="Edit user"
                    className="btn"
                  >
                    <FontAwesomeIcon icon={['fa', 'edit']} />
                  </button>
                  <button
                    onClick={() => onDeleteClick(user.id)}
                    type="button"
                    title="Delete user"
                    className="btn btn--danger"
                  >
                    <FontAwesomeIcon
                      style={{ color: '#ffffff' }}
                      icon={['fa', 'trash']}
                    />
                  </button>
                </td>
              </tr>
            ))
          : null}
      </tbody>
    </table>
  );
}
