import React from 'react';

const Users = React.lazy(() => import('./index'));

export default function LoadableUsers() {
  return (
    <React.Suspense fallback={<h2>Loading component...</h2>}>
      <Users />
    </React.Suspense>
  );
}
