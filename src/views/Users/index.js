import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import API from 'api';
import Toast from 'components/Toast';
import Pagination from 'components/Pagination';
import List from './List';

const perPage = 20;

function Users() {
  const history = useHistory();

  const [message, setMessage] = useState('');

  const [users, setUsers] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const [pageCount, setPageCount] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [offset, setOffset] = useState(0);
  const [filters, setFilters] = useState({
    first_name: '',
    last_name: '',
    gender: '',
    dob: '',
    email: '',
    phone: '',
    website: '',
    address: '',
    status: '',
  });

  const retrieveUsers = useCallback(() => {
    setLoading(true);

    API.fetchUsers({
      page: currentPage,
      ...filters,
    })
      .then((response) => {
        const { success, pageCount, message } = response._meta;
        if (success) {
          setPageCount(pageCount);
          setUsers(response.result);
          setOffset((currentPage - 1) * perPage);
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Clould not fetch users');
      })
      .finally(() => {
        setLoading(false);
      });
  }, [currentPage, filters]);

  useEffect(() => {
    retrieveUsers();
  }, [retrieveUsers]);

  useEffect(() => {
    if (!!message) {
      setTimeout(() => {
        setMessage('');
      }, 3000);
    }
  }, [message]);

  function onPageChange(currPage) {
    setCurrentPage(currPage.selected + 1);
  }

  function onNewUserClick() {
    history.push('/users/new');
  }

  function onViewClick(userId) {
    history.push(`/users/profile/${userId}`);
  }

  function onEditClick(userId) {
    history.push(`/users/edit/${userId}`);
  }

  function onDeleteClick(userId) {
    API.deleteUser(userId)
      .then((response) => {
        const { success, message } = response._meta;
        if (success) {
          retrieveUsers();
        } else {
          setMessage(message);
        }
      })
      .catch(() => {
        setMessage('Could not delete user');
      });
  }

  function onFilterChange(e) {
    if (
      e.target.tagName === 'INPUT' &&
      (e.target.type === 'text' || e.target.type === 'email') &&
      e.key === 'Enter'
    ) {
      setCurrentPage(1);
      setFilters({
        ...filters,
        [e.target.name]: e.target.value,
      });
    } else if (
      (e.target.tagName === 'INPUT' && e.target.type === 'date') ||
      e.target.tagName === 'SELECT'
    ) {
      setCurrentPage(1);
      setFilters({
        ...filters,
        [e.target.name]: e.target.value,
      });
    }
  }

  return (
    <React.Fragment>
      <Toast show={!!message} message={message} />

      <h3 className="page-title">Users</h3>

      <div className="page-header d-flex-between">
        <ol className="breadcrump">
          <li className="breadcrump__item">
            <FontAwesomeIcon icon={['fas', 'home']} />
          </li>
          <li className="breadcrump__item">Users</li>
        </ol>

        <button
          onClick={onNewUserClick}
          title="Add new user"
          className="btn btn--primary"
        >
          <FontAwesomeIcon
            style={{ marginRight: '5px' }}
            icon={['fas', 'plus']}
          />
          <span style={{ display: 'inline-block' }}>Add user</span>
        </button>
      </div>

      <div className="page-body" style={{ overflowX: 'auto' }}>
        <List
          users={users}
          isLoading={isLoading}
          offset={offset}
          onViewClick={onViewClick}
          onEditClick={onEditClick}
          onDeleteClick={onDeleteClick}
          onFilterChange={onFilterChange}
        />
      </div>
      <div className="page-footer d-flex-center">
        <Pagination
          currentPage={currentPage - 1}
          pageCount={pageCount}
          onPageChange={onPageChange}
        />
      </div>
    </React.Fragment>
  );
}

export default Users;
