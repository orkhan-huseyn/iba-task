import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Layout from 'components/Layout';
import LoadableUsers from 'views/Users/loadable';
import LoadableUserProfile from 'views/UserProfile/loadable';
import LoadableUserCreate from 'views/UserCreate/loadable';
import LoadableUserEdit from 'views/UserEdit/loadable';

function App() {
  return (
    <Switch>
      <Layout>
        <Route exact path="/" render={() => <Redirect to="/users" />} />
        <Route exact path="/users" component={LoadableUsers} />
        <Route exact path="/users/new" component={LoadableUserCreate} />
        <Route exact path="/users/edit/:userId" component={LoadableUserEdit} />
        <Route
          exact
          path="/users/profile/:userId"
          component={LoadableUserProfile}
        />
      </Layout>
    </Switch>
  );
}

export default App;
