import axios from 'axios';

/**
 * get access token from https://gorest.co.in/ and place it inside .env file
 * you can copy .env.example file, rename it to .env and paste your access token there
 */
const ACCESS_TOKEN = process.env.REACT_APP_API_ACCESS_TOKEN;

const axiosInstance = axios.create({
  baseURL: 'https://gorest.co.in/public-api/',
  timeout: 10000,
  headers: {
    Authorization: `Bearer ${ACCESS_TOKEN}`,
  },
});

export default axiosInstance;
