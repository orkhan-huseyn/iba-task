# IBA Task User and Posts

Take a look at live application demo here: [http://46.101.153.121/](http://46.101.153.121/)
Stack: React, [GoRest](https://gorest.co.in/) public api.

## Prerequisites

To run this application you must have Nodejs v12 or higher installed on your machine.

## Instructions

### Development

To run it locally you can use `yarn start` command. It will start React application in development mode.
To use the Gorest API you must get an access token from [GoRest website](https://gorest.co.in/) then add it to environment file.
Run `cp .env.example .env` to create environment file from example and place your access token there: `REACT_APP_API_ACCESS_TOKEN="YOUR-ACCESS-TOKEN"`.

Then on your browser, go to `http://localhost:3000` to take a look at the app.

### Production build

For running production build of React application, use `yarn build`. It will build React application and output static files. Then to serve static files use some CLI server like [`http-server`](https://www.npmjs.com/package/http-server).

### Docker build

Use `docker build -t orkhanhuseynli/iba-task .` to build Docker image then use `docker run -p 80:80 -d orkhanhuseynli/iba-task` to run the container.

Then go to `http://localhost` on your browser to look at the app.

## Details

### Lighthouse test results

Take a look at it on [Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2F46.101.153.121%2F).

- Performance: 96% on Mobile, 99% on Desktop
- Accessibility: 97%
- SEO: 100%
- Best Practices: 86%
- PWA - This is not a progressive web app
